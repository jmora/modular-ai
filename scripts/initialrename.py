#!/usr/bin/python3

from shutil import move
import os

fileName = 'whatever'

bigString = (
  '[FormatInfo]\n'
  'Type=TeXnicCenterProjectInformation\n'
  'Version=4\n'
  '\n'
  '[ProjectInfo]\n'
  'MainFile={fileName}.tex\n'
  'UseBibTeX=1\n'
  'UseMakeIndex=0\n'
  'ActiveProfile=LaTeX ⇨ PDF\n'
  'ProjectLanguage=en\n'
  'ProjectDialect=US\n'
)

if __name__ == "__main__":
  os.chdir('..')
  with open(fileName + '.tcp', 'w', encoding = 'utf-8') as f:
    f.write(bigString.format(fileName = fileName))
  move('paper_template.tex', fileName + '.tex')
